# ipsc-bus-client-java

`IPSC` 服务程序数据总线客户端的 `Java` 库，它基于其 `C` 库，使用 `Jni` “包装”。

由两个项目构成

1. `cpp` - `JNI` 的 `C++` 部分
2. `java` - `JNI` 的 `Java` 部分

具体请看这两个项目各自的 README
